resource "google_compute_disk" "volume" {
  name    = "src-${var.workspace_id}"
  type    = var.volume_type
  zone    = var.zone
  project = var.google_project
  size    = var.volume_size
  labels  = {
    subscription       = lower(var.subscription),
    application_type   = lower(var.application_type),
    cloud_type         = lower(var.cloud_type),
    resource_type      = lower(var.resource_type),
    subscription_group = lower(var.subscription_group),
    workspace_id       = lower(var.workspace_id),
  }
}
