output "id" {
  value = google_compute_disk.volume.id
}

output "volume_id" {
  value = google_compute_disk.volume.id
}
