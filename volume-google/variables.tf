# Variables for resource configuration
variable volume_size {}
variable volume_type {}
variable region {}
variable zone {}

# Google-specific variables
variable google_project {}
# encoded in base64
variable google_credentials {}
